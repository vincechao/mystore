import React, { Component, ReactElement, FormEvent } from "react";
import { Redirect } from "react-router-dom";
import { Form, Input, Icon, Button } from "antd";
import _ from "lodash";

import { setToken, isLogged } from "../../util";

import "./index.css";

interface LoginState {
    hasBadCredentials: boolean;
    credentials?: {
        store: string;
        username: string;
        password: string;
    };
}

export default class Login extends Component<any, LoginState> {
    public state: LoginState = {
        hasBadCredentials: false,
    };
    private WrappedForm: any;

    public componentDidMount() {
        fetch("/mock/login.json")
            .then((r) => r.json())
            .then((credentials) => {
                this.setState({ credentials });
            });
    }

    constructor(props: any) {
        super(props);
        this.WrappedForm = Form.create({ name: "login" })(this.Form);
    }

    public render(): JSX.Element {
        if (isLogged()) {
            return <Redirect to="/" />;
        }
        const { credentials } = this.state;

        return (
            <div id="login">
                <this.WrappedForm
                    handleSubmit={(
                        e: FormEvent<HTMLFormElement>,
                        form: any
                    ) => {
                        e.preventDefault();
                        form.validateFields((err: any, values: any) => {
                            if (!err) {
                                if (_.isEqual(values, credentials)) {
                                    setToken();
                                    this.forceUpdate();
                                } else {
                                    this.setState({ hasBadCredentials: true });
                                }
                            }
                        });
                    }}
                />
            </div>
        );
    }

    private Form = (props: any): ReactElement => {
        const {
            form: { getFieldDecorator },
            handleSubmit,
        } = props;
        const { hasBadCredentials } = this.state;
        return (
            <Form
                onChange={(): void => {
                    if (hasBadCredentials)
                        this.setState({ hasBadCredentials: false });
                }}
                onSubmit={(e) => handleSubmit(e, props.form)}
                className="login-form"
            >
                <Form.Item>
                    {getFieldDecorator("store", {
                        rules: [
                            {
                                required: true,
                                message: "請輸入商店代號",
                            },
                        ],
                    })(
                        <Input
                            prefix={
                                <Icon
                                    type="home"
                                    style={{ color: "rgba(0,0,0,.25)" }}
                                />
                            }
                            placeholder="商店代號5~20碼"
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator("username", {
                        rules: [
                            {
                                required: true,
                                message: "請輸入使用者帳號",
                            },
                        ],
                    })(
                        <Input
                            prefix={
                                <Icon
                                    type="user"
                                    style={{ color: "rgba(0,0,0,.25)" }}
                                />
                            }
                            placeholder="使用者帳號3~20碼"
                        />
                    )}
                </Form.Item>
                <Form.Item
                    extra={
                        hasBadCredentials ? (
                            <div style={{ color: "red" }}>帳號或密碼不正確</div>
                        ) : undefined
                    }
                >
                    {getFieldDecorator("password", {
                        rules: [
                            {
                                required: true,
                                message: "請輸入密碼",
                            },
                        ],
                    })(
                        <Input.Password
                            prefix={
                                <Icon
                                    type="lock"
                                    style={{ color: "rgba(0,0,0,.25)" }}
                                />
                            }
                            placeholder="密碼6~20碼，英文字母需區分大小寫"
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                    >
                        登入
                    </Button>
                </Form.Item>
            </Form>
        );
    };
}
