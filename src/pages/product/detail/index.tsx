import React, { Component, ReactElement } from "react";
import _ from "lodash";
import { Link, Redirect } from "react-router-dom";

import { ProductContext, Labels } from "..";

import "./index.css";

export default class Login extends Component<any, any> {
    public render(): JSX.Element {
        const { match } = this.props;
        const id = match?.params?.id;

        return (
            <ProductContext.Consumer>
                {({ products }): ReactElement => {
                    if (_.isUndefined(products?.[id])) {
                        return <Redirect to="/" />;
                    }

                    return (
                        <div id="detail">
                            {_.map(
                                _.omit(products?.[id], ["id"]),
                                (value, key): ReactElement => (
                                    <div key={key} className="row">
                                        <span className="label">
                                            {Labels[key]}
                                        </span>
                                        <span className="content">{value}</span>
                                    </div>
                                )
                            )}
                            <Link to="/product/overview">返回</Link>
                        </div>
                    );
                }}
            </ProductContext.Consumer>
        );
    }
}
