import React, { Component, ReactElement } from "react";
import _ from "lodash";
import { Input } from "antd";
import { Link, Redirect } from "react-router-dom";

import { ProductContext, ProductData, Labels } from "..";

import "./index.css";

export default class Login extends Component<any, any> {
    private newValues = {} as Partial<ProductData>;

    public render(): JSX.Element {
        const { match } = this.props;
        const id = match?.params?.id;

        return (
            <ProductContext.Consumer>
                {({ products, onProductUpdated }): ReactElement => {
                    if (_.isUndefined(products?.[id])) {
                        return <Redirect to="/" />;
                    }

                    return (
                        <div id="edit">
                            {_.map(
                                _.omit(products?.[id], ["id"]),
                                (value, key): ReactElement => (
                                    <div className="row" key={key}>
                                        <span>{Labels[key]}</span>
                                        <Input
                                            defaultValue={value}
                                            onChange={(e): void => {
                                                this.newValues = _.assign(
                                                    this.newValues,
                                                    {
                                                        [key]: e.target.value,
                                                    }
                                                );
                                            }}
                                        />
                                    </div>
                                )
                            )}
                            <Link
                                onClick={(): void => {
                                    if (!!onProductUpdated) {
                                        onProductUpdated(
                                            _.assign(this.newValues, { id })
                                        );
                                    }
                                }}
                                to={`/product/detail/${id}`}
                            >
                                儲存
                            </Link>
                        </div>
                    );
                }}
            </ProductContext.Consumer>
        );
    }
}
