import React, { Component, ReactElement } from "react";
import { Button, Input, Table } from "antd";
import _ from "lodash";
import { SortOrder } from "antd/lib/table";

import { ProductContext, Labels } from "..";

import "./index.css";
import { Link } from "react-router-dom";

interface OverviewState {
    searchProductId?: string;
    searchName?: string;
}

export default class Overview extends Component<any, OverviewState> {
    public state: OverviewState = {};
    private searchProductId = "";
    private searchName = "";

    public render(): JSX.Element {
        const { searchProductId, searchName } = this.state;

        const columns = [
            { key: "brand", dataIndex: "brand", title: Labels["brand"] },
            {
                key: "name",
                dataIndex: "name",
                title: Labels["name"],
                sorter: (a: any, b: any) => a.name.length - b.name.length,
                sortDirections: ["descend", "ascend"] as SortOrder[],
            },
            {
                key: "productId",
                dataIndex: "productId",
                title: Labels["productId"],
                sorter: (a: any, b: any) =>
                    a.productId.length - b.productId.length,
                sortDirections: ["descend", "ascend"] as SortOrder[],
            },
            {
                key: "price",
                dataIndex: "price",
                title: Labels["price"],
                sorter: (a: any, b: any): number => a.price - b.price,
                sortDirections: ["descend", "ascend"] as SortOrder[],
            },
            { key: "color", dataIndex: "color", title: Labels["color"] },
            { key: "size", dataIndex: "size", title: Labels["size"] },
            {
                key: "action",
                dataIndex: "id",
                title: "動作",
                render: (text: any, record: any) => (
                    <div>
                        <Link to={`/product/detail/${record.id}`}>檢視</Link>
                        <Link
                            to={`/product/edit/${record.id}`}
                            style={{ marginLeft: "5px" }}
                        >
                            編輯
                        </Link>
                    </div>
                ),
            },
        ];

        return (
            <ProductContext.Consumer>
                {({ products }): ReactElement => {
                    return (
                        <div id="overview">
                            <div className="search">
                                <div className="inputs">
                                    <div>商品編號</div>
                                    <Input
                                        placeholder="請輸入"
                                        defaultValue={searchProductId}
                                        onChange={(e): void => {
                                            this.searchProductId =
                                                e.target.value;
                                        }}
                                    />
                                    <div>商品名稱</div>
                                    <Input
                                        placeholder="請輸入"
                                        defaultValue={searchName}
                                        onChange={(e): void => {
                                            this.searchName = e.target.value;
                                        }}
                                    />
                                </div>
                                <Button
                                    icon="search"
                                    onClick={(): void => {
                                        this.setState({
                                            searchProductId: this
                                                .searchProductId,
                                            searchName: this.searchName,
                                        });
                                    }}
                                >
                                    搜尋
                                </Button>
                            </div>
                            <Table
                                dataSource={_.filter(
                                    products,
                                    (product): boolean => {
                                        return (
                                            _.includes(
                                                _.toLower(product.name),
                                                _.toLower(searchName)
                                            ) &&
                                            _.includes(
                                                _.toLower(product.productId),
                                                _.toLower(searchProductId)
                                            )
                                        );
                                    }
                                )}
                                columns={columns}
                                pagination={{
                                    showSizeChanger: true,
                                    showQuickJumper: true,
                                    defaultPageSize: 20,
                                    pageSizeOptions: ["20", "40", "100"],
                                }}
                                rowKey="id"
                            />
                        </div>
                    );
                }}
            </ProductContext.Consumer>
        );
    }
}
