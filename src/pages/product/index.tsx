import React, { Component, createContext } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import _ from "lodash";

import Overview from "./overview";
import Detail from "./detail";
import Edit from "./edit";

export const ProductContext = createContext<{
    products?: ProductData[];
    onProductUpdated?: (newValues: any) => void;
}>({
    products: undefined,
    onProductUpdated: undefined,
});

export const Labels: { [id: string]: string } = {
    brand: "品牌名稱",
    name: "商品名稱",
    productId: "商品編號",
    price: "價格",
    color: "規格1 (顏色)",
    size: "規格2 (大小)",
};

export interface ProductData {
    brand: string;
    name: string;
    id: string;
    productId: string;
    price: string;
    color: string;
    size: string;
}

interface LoginState {
    products?: ProductData[];
}

export default class Product extends Component<any, LoginState> {
    public state: LoginState = {};

    public componentDidMount(): void {
        fetch("/mock/product.json")
            .then((r) => r.json())
            .then((product) => {
                const products = _.map(product?.data, (p, index): any => ({
                    ...p,
                    id: `${index}`,
                    productId: p.id,
                }));
                this.setState({ products });
            });
    }

    public render(): JSX.Element {
        const { match } = this.props;
        return (
            <ProductContext.Provider
                value={{
                    ...this.state,
                    onProductUpdated: this.onProductUpdated,
                }}
            >
                <Router>
                    <Switch>
                        <Route
                            path={`${match?.path}/overview`}
                            component={Overview}
                        />
                        <Route
                            path={`${match?.path}/detail/:id`}
                            component={Detail}
                        />
                        <Route
                            path={`${match?.path}/edit/:id`}
                            component={Edit}
                        />
                        <Route path="*">
                            <Redirect from="/" to={`${match?.path}/overview`} />
                        </Route>
                    </Switch>
                </Router>
            </ProductContext.Provider>
        );
    }

    private onProductUpdated = (newValues: Partial<ProductData>): void => {
        const { products } = this.state;
        const target = _.find(products, { id: newValues.id });
        _.assign(target, newValues);
        console.log(target);
        this.forceUpdate();
    };
}
