import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";

import SecurityRoute from "./SecurityRoute";
import Login from "./pages/login";
import Product from "./pages/product";

import "./App.css";

class App extends Component<any, any> {
    public render(): JSX.Element {
        return (
            <Router>
                <Switch>
                    <Route path="/login" component={Login} />
                    <SecurityRoute path="/product" component={Product} />
                    <Route exact path="/">
                        <Redirect exact from="/" to="product" />
                    </Route>
                    <Route path="*">
                        <Redirect from="/" to="product" />
                    </Route>
                </Switch>
            </Router>
        );
    }
}

export default App;
