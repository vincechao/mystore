export const isLogged = (): boolean => {
    const token = localStorage.getItem("token");
    return token === "ALLOW";
};

export const setToken = (): void => {
    localStorage.setItem("token", "ALLOW");
};
