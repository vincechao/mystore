import React, { Component, ReactElement } from "react";
import { Route, Redirect } from "react-router-dom";

import { isLogged } from "./util";

export default class SecurityRoute extends Component<any, any> {
    public render(): JSX.Element {
        const { component, ...rest } = this.props;
        const Children = component;
        return (
            <Route
                {...rest}
                render={(props: any) =>
                    isLogged() ? (
                        <Children {...props} />
                    ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: { from: props.location },
                            }}
                        />
                    )
                }
            />
        );
    }
}
